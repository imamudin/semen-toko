package com.muslimtekno.bigtoko;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.muslimtekno.bigtoko.config.GlobalConfig;
import com.muslimtekno.bigtoko.model.Pembayaran;
import com.muslimtekno.bigtoko.mysp.ObscuredSharedPreferences;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
/**
 * Created by imamudin on 03/04/17.
 */
public class DetailPembayaran extends AppCompatActivity {
    LinearLayout ll_main;
    ObscuredSharedPreferences pref;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_pembayaran);

        init();
    }
    private void init(){
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Data Pembayaran");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        pref = new ObscuredSharedPreferences(DetailPembayaran.this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );

        TextView jenis_pembayaran   = (TextView) findViewById(R.id.tv_pembayaran);
        TextView no_kuitansi        = (TextView) findViewById(R.id.tv_no_kuitansi);
        TextView tgl_kuitansi       = (TextView) findViewById(R.id.tv_tgl_kuitansi);
        TextView no_faktur          = (TextView) findViewById(R.id.tv_no_faktur);
        TextView tgl_faktur         = (TextView) findViewById(R.id.tv_tgl_faktur);
        TextView total              = (TextView) findViewById(R.id.tv_total_pembayaran);

        Intent old  = getIntent();
        String datString = old.getStringExtra("data");
        try {
            JSONObject bayar = new JSONObject(datString);


            Pembayaran m = new Pembayaran();
            m.pembayaranJson = bayar.toString();
            m.NAMA_PELANGGAN = bayar.getString(GlobalConfig.PNAMA_PELANGGAN);
            m.ALAMAT_PELANGGAN = bayar.getString(GlobalConfig.PALAMAT_PELANGGAN);
            m.PEMBAYARAN = bayar.getString(GlobalConfig.PPEMBAYARAN);
            m.NO_KUITANSI = bayar.getString(GlobalConfig.PNO_KUITANSI);
            m.TGL_KUITANSI = bayar.getString(GlobalConfig.PTGL_KUITANSI);
            m.NO_FAKTUR = bayar.getString(GlobalConfig.PNO_FAKTUR);
            m.TGL_FAKTUR = bayar.getString(GlobalConfig.PTGL_FAKTUR);
            m.TOTAL = bayar.getString(GlobalConfig.PTOTAL);

            jenis_pembayaran.setText(m.PEMBAYARAN);
            no_kuitansi.setText(m.NO_KUITANSI);
            tgl_kuitansi.setText(m.TGL_KUITANSI);
            no_faktur.setText(m.NO_FAKTUR);
            tgl_faktur.setText(m.TGL_FAKTUR);
            total.setText(decimalToRupiah(Double.parseDouble(m.TOTAL)));
        }catch (Exception e){
            Log.i(getLocalClassName(), "tidak dapat memproses json objek");
        }
        ll_main     = (LinearLayout)findViewById(R.id.ll_main);
    }

    private String decimalToRupiah(double harga){
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);

        String hasil = kursIndonesia.format(harga);
        return hasil.substring(0, (hasil.length()-3));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //menu.add(1, 2, 1, "Hapus Filter").setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
