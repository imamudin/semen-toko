package com.muslimtekno.bigtoko;

/**
 * Created by imamudin on 12/01/17.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.muslimtekno.bigtoko.app.MyAppController;
import com.muslimtekno.bigtoko.config.GlobalConfig;
import com.muslimtekno.bigtoko.mysp.ObscuredSharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Map;

public class Dashboard extends AppCompatActivity{
    LinearLayout ll_main;
    Button btn_lokasi;
    TextView tv_koordinat, tv_nama_toko, tv_nama_sales, tv_alamat_toko, tv_palfon;
    TextView tv_jatuh_tempo_bayar, tv_total_piutang, tv_tagihan_terdekat;
    LinearLayout ll_history_pembayaran, ll_hOrder, ll_hKunjungan, ll_piutang, ll_poin, ll_logout, ll_pelunasan;
    boolean firt_time;
    ObscuredSharedPreferences pref;
    ProgressDialog loading;
    JsonObjectRequest request;

    //untuk filter
    LinearLayout ll_tgl_mulai, ll_tgl_selesai;
    Button btn_simpan, btn_kosongkan;
    TextView tv_tgl_mulai, tv_tgl_selesai;
    Calendar c_awal, c_akhir;
    AlertDialog alert_detail_piutang;
    //tutup untuk filter

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);

        init();
        init_filter();
        setTanggalHariIni();
        updateProfil();
    }
    private void init(){
        firt_time = true;
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.app_name));
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(false);

        pref = new ObscuredSharedPreferences(Dashboard.this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );

        if(!pref.getBoolean(GlobalConfig.IS_LOGIN, false)){
            openLogin();
            Log.d("login", "buka main");
        }
        if(pref.getBoolean(GlobalConfig.IS_TOKO, false)){
            openToko();
            Log.d("toko", "buka toko");
        }


        ll_history_pembayaran= (LinearLayout)findViewById(R.id.ll_history_pembayaran);
        ll_hKunjungan   = (LinearLayout)findViewById(R.id.ll_kunjungan);
        ll_piutang      = (LinearLayout)findViewById(R.id.ll_piutang);
        ll_hOrder       = (LinearLayout)findViewById(R.id.ll_order);
        ll_poin         = (LinearLayout)findViewById(R.id.ll_poin);
        ll_logout       = (LinearLayout)findViewById(R.id.ll_logout);
        ll_pelunasan    = (LinearLayout)findViewById(R.id.ll_pelunasan);


        btn_lokasi      = (Button)findViewById(R.id.btn_lokasi);
        tv_koordinat    = (TextView)findViewById(R.id.tv_koordinat);

        //untuk menampilkan target
        tv_nama_toko    = (TextView)findViewById(R.id.tv_nama_toko);
        tv_alamat_toko  = (TextView)findViewById(R.id.tv_alamat_toko);
        tv_palfon       = (TextView)findViewById(R.id.tv_palfon);
        tv_jatuh_tempo_bayar    = (TextView)findViewById(R.id.tv_jatuh_tempo_bayar);
        tv_nama_sales   = (TextView)findViewById(R.id.tv_nama_sales);
        tv_total_piutang= (TextView)findViewById(R.id.tv_total_piutang);
        tv_tagihan_terdekat     = (TextView)findViewById(R.id.tv_tagihan_terdekat);

        ll_history_pembayaran.setOnClickListener(btnClick);
        ll_hKunjungan.setOnClickListener(btnClick);
        ll_hOrder.setOnClickListener(btnClick);
        ll_piutang.setOnClickListener(btnClick);
        ll_poin.setOnClickListener(btnClick);
        ll_logout.setOnClickListener(btnClick);
        btn_lokasi.setOnClickListener(btnClick);
        ll_pelunasan.setOnClickListener(btnClick);

        ll_main     = (LinearLayout)findViewById(R.id.ll_main);
    }
    private void init_filter(){
        ll_tgl_mulai    = (LinearLayout)findViewById(R.id.ll_tgl_mulai);
        ll_tgl_selesai  = (LinearLayout)findViewById(R.id.ll_tgl_selesai);

        btn_kosongkan   = (Button) findViewById(R.id.btn_kosongkan);
        btn_simpan      = (Button) findViewById(R.id.btn_simpan);

        tv_tgl_mulai    = (TextView) findViewById(R.id.tv_tgl_mulai);
        tv_tgl_selesai  = (TextView) findViewById(R.id.tv_tgl_selesai);

        tv_tgl_mulai.setText(pref.getString(GlobalConfig.FILTER_TGL_MULAI_S,"-"));
        tv_tgl_selesai.setText(pref.getString(GlobalConfig.FILTER_TGL_AKHIR_S,"-"));

        ll_tgl_mulai.setOnClickListener(btnClickfilter);
        ll_tgl_selesai.setOnClickListener(btnClickfilter);
        btn_kosongkan.setOnClickListener(btnClickfilter);
        btn_simpan.setOnClickListener(btnClickfilter);

        //inisialia awal dilai calendar jika pref sudah ada
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Log.i(GlobalConfig.TAG, "Set tanggal awal.");
            setTanggalHariIni();
        }catch (Exception e){
            Log.i(GlobalConfig.TAG, "tidak dapat memproses tanggal.");
        }

        if(!pref.getString(GlobalConfig.FILTER_TOKOID, "").equals("")){
            tv_nama_toko.setText(pref.getString(GlobalConfig.FILTER_TOKO_NAMA,""));
        }
    }

    View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent open = null;
            if(v==ll_history_pembayaran){
                open = new Intent(Dashboard.this, HistoryPembayaran.class);
            }else if(v==ll_hKunjungan){
                open = new Intent(Dashboard.this, HistoryKunjungan.class);
            }else if(v==ll_hOrder){
                open = new Intent(Dashboard.this, HistoryOrder.class);
            }else if(v==ll_piutang){
                open = new Intent(Dashboard.this, DaftarPiutang.class);
            }else if(v==ll_poin){
                getPoin();
                return;
            }else if(v==ll_logout){
                logout();
                return;
            }else if(v==ll_pelunasan){
                getDetailPiutang();
                return;
            }
            else if(v==btn_lokasi){
                return;
            }
            if(open != null){
                startActivity(open);
            }else{
                notifikasi("Mohon maaf terjadi kesalahan.");
            }
        }
    };
    View.OnClickListener btnClickfilter = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v==btn_simpan){
                simpan();
                return;
            }else if(v==btn_kosongkan){
                setTanggalHariIni();

                Toast.makeText(Dashboard.this, "Filter telah dihapus.", Toast.LENGTH_SHORT).show();
                return;
            }else if(v==ll_tgl_mulai){
                dateTimePick(tv_tgl_mulai);
                return;
            }else if(v==ll_tgl_selesai){
                dateTimePick(tv_tgl_selesai);
                return;
            }
        }
    };
    private void  simpan(){
        if(c_awal == null || c_akhir==null || tv_tgl_mulai.getText().equals("-") || tv_tgl_selesai.getText().equals("-")){
            //notifikasi("Masukan tanggal mulai dan akhir.");
        }else {
            if (c_awal.getTimeInMillis() > c_akhir.getTimeInMillis()) {
                notifikasi("Tanggal mulai harus lebih rendah.");
            } else {View.OnClickListener btnClickfilter = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(v==btn_simpan){
                        simpan();
                        return;
                    }else if(v==btn_kosongkan){
                        Calendar c = Calendar.getInstance();

                        Calendar cal_mulai = Calendar.getInstance();
                        cal_mulai.add(Calendar.MONTH, -1);
                        cal_mulai.add(Calendar.DATE, 1);

                        SimpleDateFormat format_s   = new SimpleDateFormat("dd MMM yyyy");
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

                        tv_tgl_mulai.setText(""+format_s.format(cal_mulai.getTime()));
                        tv_tgl_selesai.setText(""+format_s.format(c.getTime()));

                        pref.edit().remove(GlobalConfig.FILTER_TOKOID).commit();
                        pref.edit().remove(GlobalConfig.FILTER_TOKO_NAMA).commit();
                        pref.edit().remove(GlobalConfig.FILTER_TOKO_ALAMAT).commit();

                        pref.edit().putString(GlobalConfig.FILTER_TGL_MULAI, ""+format.format(c_awal.getTime())).commit();
                        pref.edit().putString(GlobalConfig.FILTER_TGL_AKHIR, ""+format.format(c_akhir.getTime())).commit();
                        pref.edit().putString(GlobalConfig.FILTER_TGL_MULAI_S, ""+tv_tgl_mulai.getText()).commit();
                        pref.edit().putString(GlobalConfig.FILTER_TGL_AKHIR_S, ""+tv_tgl_selesai.getText()).commit();

                        setTanggalHariIni();

                        Toast.makeText(Dashboard.this, "Filter telah dihapus.", Toast.LENGTH_SHORT).show();

//                setResult(RESULT_OK);
//                finish();
                        return;
                    }else if(v==ll_tgl_mulai){
                        dateTimePick(tv_tgl_mulai);
                        return;
                    }else if(v==ll_tgl_selesai){
                        dateTimePick(tv_tgl_selesai);
                        return;
                    }
                }
            };

                Log.i(GlobalConfig.TAG, tv_tgl_mulai.getText()+" === "+tv_tgl_selesai.getText());

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                Log.i(GlobalConfig.TAG, "tanggal : "+format.format(c_awal.getTime())+", akhir : "+format.format(c_akhir.getTime()));

                pref.edit().putString(GlobalConfig.FILTER_TGL_MULAI, ""+format.format(c_awal.getTime())).commit();
                pref.edit().putString(GlobalConfig.FILTER_TGL_AKHIR, ""+format.format(c_akhir.getTime())).commit();
                pref.edit().putString(GlobalConfig.FILTER_TGL_MULAI_S, ""+tv_tgl_mulai.getText()).commit();
                pref.edit().putString(GlobalConfig.FILTER_TGL_AKHIR_S, ""+tv_tgl_selesai.getText()).commit();
            }
            Toast.makeText(Dashboard.this, "Filter telah disimpan.", Toast.LENGTH_SHORT).show();
        }
        //setResult(RESULT_OK);
        //finish();
    }
    private void dateTimePick(final TextView editText){
        final View dialogView = View.inflate(this, R.layout.date_picker, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        final DatePicker dp_awal = (DatePicker) dialogView.findViewById(R.id.date_picker);
        dp_awal.setMaxDate(System.currentTimeMillis());
        if(editText==tv_tgl_mulai){
            dp_awal.updateDate(c_awal.get(Calendar.YEAR),c_awal.get(Calendar.MONTH),c_awal.get(Calendar.DATE));
        }else {
            dp_awal.updateDate(c_akhir.get(Calendar.YEAR),c_akhir.get(Calendar.MONTH),c_akhir.get(Calendar.DATE));
            if (!tv_tgl_mulai.getText().toString().trim().equals("")) {
                String mytime = tv_tgl_mulai.getText().toString().trim();
                Log.i(getLocalClassName(), mytime);
                SimpleDateFormat dateFormat = new SimpleDateFormat(
                        "dd MMM yyyy");
                Date myDate = null;
                try {
                    myDate = dateFormat.parse(mytime);
                    dp_awal.setMinDate(myDate.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                    Log.i(getLocalClassName(), "error");
                }
            }else{
                Log.i(getLocalClassName(), "");
                dp_awal.updateDate(c_akhir.get(Calendar.YEAR),c_akhir.get(Calendar.MONTH),c_akhir.get(Calendar.DATE));
            }
        }

        dialogView.findViewById(R.id.date_time_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = new GregorianCalendar(dp_awal.getYear(),
                        dp_awal.getMonth(),
                        dp_awal.getDayOfMonth());

                if(editText==tv_tgl_mulai){
                    c_awal = cal;
                }else{
                    c_akhir = cal;
                }

                SimpleDateFormat format_show = new SimpleDateFormat("dd MMM yyyy");
                Log.i(GlobalConfig.TAG, "tanggal : "+format_show.format(cal.getTime()));
                editText.setText(format_show.format(cal.getTime()));

                //menyimpan perubahan tanggal dan nama toko
                simpan();

                alertDialog.dismiss();
            }});
        alertDialog.setView(dialogView);
        alertDialog.show();
    }
    public void updateProfil(){
        Log.d(getLocalClassName(), "update target");

        tv_koordinat.setText(pref.getString(GlobalConfig.GTOKO_LAT,"")+";"+pref.getString(GlobalConfig.GTOKO_LONG,""));
        tv_nama_toko.setText(pref.getString(GlobalConfig.GT_NAMA,""));
        tv_alamat_toko.setText(pref.getString(GlobalConfig.GT_ALAMAT,""));
        tv_palfon.setText(decimalToRupiah(Double.parseDouble(pref.getString(GlobalConfig.GREAL_PLAFON,"0"))));
        tv_jatuh_tempo_bayar.setText(pref.getString(GlobalConfig.GTGL_JATUH_TEMPO,"-"));
        tv_nama_sales.setText(pref.getString(GlobalConfig.GKNAMA_SALES,"-"));

        tv_total_piutang.setText(decimalToRupiah(Double.parseDouble(pref.getString(GlobalConfig.GTOTAL_PIUTANG,"0"))));
        tv_tagihan_terdekat.setText(decimalToRupiah(Double.parseDouble(pref.getString(GlobalConfig.GTAGIHAN_TERDEKAT,"0"))));
    }
    private String decimalToRupiah(double harga){
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);

        String hasil = kursIndonesia.format(harga);
        return hasil.substring(0, (hasil.length()-3));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(1, 1, 1, "Update Data Pelanggan").setIcon(R.drawable.ic_refresh2).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
//        menu.add(2, 2, 2, "Logout").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        return super.onCreateOptionsMenu(menu);
    }
    public void displayMessage(String toastString){
        Toast.makeText(getApplicationContext(), toastString, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case 1:
                getTarget();
                return true;
            case 2:
                dialogLogout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void getPoin(){
        loading = ProgressDialog.show(Dashboard.this, "", "Mohon tunggu...", true);

        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_POINS;
        //Log.d(GlobalConfig.TAG,""+url);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.UP_KODE, pref.getString(GlobalConfig.GT_KODE, ""));
            jsonBody.put(GlobalConfig.USER_REGID, pref.getString(GlobalConfig.USER_REGID, ""));

            jsonBody.put(GlobalConfig.UP_START, 30);
            jsonBody.put(GlobalConfig.UP_LIMIT, GlobalConfig.MAX_ROW_PER_REQUEST);

            //Log.d(GlobalConfig.TAG, jsonBody.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    Log.d(GlobalConfig.TAG, "tutup loading");
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            //hapus semua preferences
                            JSONObject data   = response.getJSONObject("data");

                            Intent poinToko = new Intent(Dashboard.this, PoinToko.class);
                            poinToko.putExtra("data", data.toString());

                            startActivity(poinToko);
                        }else{
                            notifikasi(message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //Log.d("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // do something
                    loading.dismiss();
                    if(error!=null) {
                        //Log.d("respons", error.getMessage().toString());
                    }
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void getTarget(){
        loading = ProgressDialog.show(Dashboard.this, "", "Mohon tunggu...", true);

        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_DATAPELANGGAN;
        //Log.d(GlobalConfig.TAG,""+url);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.UP_KODE, pref.getString(GlobalConfig.GT_KODE, ""));
            jsonBody.put(GlobalConfig.USER_REGID, pref.getString(GlobalConfig.USER_REGID, ""));

            //Log.d(GlobalConfig.TAG, jsonBody.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    Log.d(GlobalConfig.TAG, "tutup loading");
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            //hapus semua preferences
                            JSONObject data     = response.getJSONObject("data");

                            pref.edit().putString(GlobalConfig.GT_NAMA, data.getString(GlobalConfig.GT_NAMA)).commit();
                            pref.edit().putString(GlobalConfig.GT_ALAMAT, data.getString(GlobalConfig.GT_ALAMAT)).commit();
                            pref.edit().putString(GlobalConfig.GREAL_PLAFON, data.getString(GlobalConfig.GREAL_PLAFON)).commit();
                            pref.edit().putString(GlobalConfig.GKDSALES, data.getString(GlobalConfig.GKDSALES)).commit();
                            pref.edit().putString(GlobalConfig.GTOKO_LAT, data.getString(GlobalConfig.GTOKO_LAT)).commit();
                            pref.edit().putString(GlobalConfig.GTOKO_LONG, data.getString(GlobalConfig.GTOKO_LONG)).commit();
                            pref.edit().putString(GlobalConfig.GKNAMA_SALES, data.getString(GlobalConfig.GKNAMA_SALES)).commit();
                            pref.edit().putString(GlobalConfig.GJATUHTEMPOBAYAR, data.getString(GlobalConfig.GJATUHTEMPOBAYAR)).commit();

                            Toast.makeText(getApplicationContext(),"Data Pelanggan telah diperbarui.",Toast.LENGTH_LONG).show();
                            updateProfil();
                        }else{
                            notifikasi(message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //Log.d("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // do something
                    loading.dismiss();
                    if(error!=null) {
                        //Log.d("respons", error.getMessage().toString());
                    }
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void notifikasi(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
    }
    private void dialogLogout(){
        AlertDialog.Builder builder = new AlertDialog.Builder(Dashboard.this);
        // Set the dialog title
        builder.setTitle(getResources().getString(R.string.app_name))
                .setMessage("Apakah yakin anda akan keluar?")
                .setCancelable(false)
                .setNegativeButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        logout();
                    }
                })
                .setPositiveButton("Batal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
        ;
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    private void logout(){
        Log.d(GlobalConfig.TAG, "make progress dialog2");
        loading = ProgressDialog.show(Dashboard.this, "Logout...", "Mohon tunggu...", true);

        String url = "";
        if(pref.getString(GlobalConfig.IP_KEY, null) != null){
            url = pref.getString(GlobalConfig.IP_KEY, "")+GlobalConfig.WEB_URL+GlobalConfig.URL_LOGOUT;
        }else{
            url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_LOGOUT;
        }
        //Log.d(GlobalConfig.TAG,""+url);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.GT_KODE, pref.getString(GlobalConfig.GT_KODE, ""));
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));

            //Log.d(GlobalConfig.TAG, jsonBody.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    Log.d(GlobalConfig.TAG, "tutup loading");
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            //hapus semua preferences
                            pref.edit().remove(GlobalConfig.USER_ID).commit();
                            pref.edit().remove(GlobalConfig.USER_TOKEN).commit();
                            pref.edit().remove(GlobalConfig.IS_LOGIN).commit();
                            openLogin();
                        }else{
                            notifikasi(message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //Log.d("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // do something
                    loading.dismiss();
                    if(error!=null) {
                        //Log.d("respons", error.getMessage().toString());
                    }
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void openLogin(){
        Intent login = new Intent(Dashboard.this, Login.class);
        startActivity(login);
        Dashboard.this.finish();
    }
    private void openToko(){
        Intent toko = new Intent(Dashboard.this, Toko.class);
        startActivity(toko);
        Dashboard.this.finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelRequest();
        if(pref.getBoolean(GlobalConfig.IS_LOGIN, false)) {
        }
    }
    private void cancelRequest(){
        if(request!=null) {
            MyAppController.getInstance().cancelPendingRequests(request);
        }
    }
    private void setTanggalHariIni(){
        Log.i(GlobalConfig.TAG, "Set tanggal awal.");
        c_awal = Calendar.getInstance();
        c_awal.set(Calendar.DATE, 1);

        c_akhir = Calendar.getInstance();

        SimpleDateFormat format_s   = new SimpleDateFormat("dd MMM yyyy");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        tv_tgl_mulai.setText(""+format_s.format(c_awal.getTime()));
        tv_tgl_selesai.setText(""+format_s.format(c_akhir.getTime()));

        pref.edit().putString(GlobalConfig.FILTER_TGL_MULAI, ""+format.format(c_awal.getTime())).commit();
        pref.edit().putString(GlobalConfig.FILTER_TGL_AKHIR, ""+format.format(c_akhir.getTime())).commit();
        pref.edit().putString(GlobalConfig.FILTER_TGL_MULAI_S, ""+tv_tgl_mulai.getText()).commit();
        pref.edit().putString(GlobalConfig.FILTER_TGL_AKHIR_S, ""+tv_tgl_selesai.getText()).commit();
    }
    private void getDetailPiutang(){
        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_PELUNASAN_TOKO;
        //Log.d(GlobalConfig.TAG,""+url);

        loading = ProgressDialog.show(Dashboard.this, "", "Mohon tunggu...", true);
        //bisa order
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.UP_KODE, pref.getString(GlobalConfig.GT_KODE, ""));
            jsonBody.put(GlobalConfig.USER_REGID, pref.getString(GlobalConfig.USER_REGID, ""));
            //Log.d(GlobalConfig.TAG, jsonBody.toString());
            request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    //Log.d(GlobalConfig.TAG, response.toString());
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            JSONObject datas = response.getJSONObject("data");
                            showDetailPiutang(datas.toString());
                        }else{
                            notifikasi(message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //Log.d("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loading.dismiss();
                    NetworkResponse response = error.networkResponse;
                    if(response != null && response.data != null){
                        Log.d(GlobalConfig.TAG, "code"+response.statusCode);
                        switch(response.statusCode){
                            case 404:
                                notifikasi("Terjadi masalah dengan server.");
                                break;
                            case 408:
                                notifikasi("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                                break;
                            case 500:
                                notifikasi("Terjadi masalah dengan server.");
                                break;
                            default:
                                notifikasi("Mohon maaf terjadi kesalahan.");
                                break;
                        }
                    }
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void showDetailPiutang(String dataString){
        LayoutInflater layoutInflater = (LayoutInflater)getLayoutInflater();
        View promptView = layoutInflater.inflate(R.layout.notif_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Dashboard.this);
        alertDialogBuilder.setView(promptView);

        final TextView t_title      = (TextView) promptView.findViewById(R.id.t_title_dialog);
        final LinearLayout ll_notif = (LinearLayout) promptView.findViewById(R.id.ll_notif);

        t_title.setText(pref.getString(GlobalConfig.FILTER_TOKO_NAMA,""));
        try {
            //Log.d(GlobalConfig.TAG, dataString);
            JSONObject data = new JSONObject(dataString);
            JSONArray datas = data.getJSONArray("piutang");
            for(int i=0;i<datas.length();i++){
                JSONObject piutang  = datas.getJSONObject(i);

                String faktur       = piutang.getString(GlobalConfig.GDP_NO_FAKTUR);
                String tanggal_faktur= piutang.getString(GlobalConfig.GDP_TGL_FAKTUR);
                String total        = piutang.getString(GlobalConfig.GDP_TOTAL);
                String jatuh_tempo  = piutang.getString(GlobalConfig.GDP_JATUH_TEMPO);
                String belum_dibayar= piutang.getString(GlobalConfig.GDP_BELUM_DIBAYAR);

                if(total.length()<=0) total ="0";

                ll_notif.addView(getViewDataPiutang(i,faktur, tanggal_faktur, decimalToRupiah(Double.parseDouble(total)), jatuh_tempo, Double.parseDouble(belum_dibayar)));
            }
        }catch (Exception e){
            //Log.d(GlobalConfig.TAG, e.getMessage().toString());
            Log.d(GlobalConfig.TAG, "Tidak dapat memproses data json.");
        }
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setNegativeButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
    private View getViewDataPiutang(int urutan, final String faktur, String tanggal_faktur, final String total, String jatuh_tempo, final double belum_dibayar){
        LayoutInflater inflater     = (LayoutInflater)this.getLayoutInflater();
        View data_history_view    = inflater.inflate(R.layout.list_piutang_dialog_button, null);

        final TextView tv_faktur        =(TextView)data_history_view.findViewById(R.id.tv_faktur);
        final TextView tv_tanggal_faktur=(TextView)data_history_view.findViewById(R.id.tv_tanggal_faktur);
        final TextView tv_total         =(TextView)data_history_view.findViewById(R.id.tv_total);
        final TextView tv_jatuh_tempo   =(TextView)data_history_view.findViewById(R.id.tv_tanggal_jatuh_tempo);
        final TextView tv_belum_dibayar =(TextView)data_history_view.findViewById(R.id.tv_belum_dibayar);
        final Button btn_bayar          = (Button) data_history_view.findViewById(R.id.btn_bayar);

        tv_faktur.setText(faktur);
        tv_tanggal_faktur.setText(tanggal_faktur);
        tv_total.setText(total);
        tv_jatuh_tempo.setText(jatuh_tempo);
        if(urutan!=0){
            btn_bayar.setVisibility(View.GONE);
        }
        tv_belum_dibayar.setText(decimalToRupiah(belum_dibayar));
        btn_bayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflater = (LayoutInflater)getLayoutInflater();
                View promptView = layoutInflater.inflate(R.layout.dialog_pembayaran, null);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Dashboard.this);
                alertDialogBuilder.setView(promptView);

                final TextView t_title      = (TextView) promptView.findViewById(R.id.t_title_dialog);
                final TextView tv_jumlah_pembayaran      = (TextView) promptView.findViewById(R.id.et_jumlah_pembayaran);
                final TextView tv_tgl_bayar = (TextView) promptView.findViewById(R.id.tv_tanggal_pembayaran);
                final TextView tv_tgl_bayar_kirim   = (TextView) promptView.findViewById(R.id.tv_tanggal_pembayaran_kirim);
                final TextView tv_faktur_pembayaran = (TextView) promptView.findViewById(R.id.tv_faktur_pembayaran);
                final LinearLayout ll_tgl_pembayaran= (LinearLayout) promptView.findViewById(R.id.ll_tgl_pembayaran);

                t_title.setText(pref.getString(GlobalConfig.FILTER_TOKO_NAMA,""));
                tv_faktur_pembayaran.setText(faktur);
                tv_jumlah_pembayaran.setText(""+(int)(belum_dibayar));
                Calendar c = Calendar.getInstance();
                SimpleDateFormat format_s   = new SimpleDateFormat("dd MMM yyyy");
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

                tv_tgl_bayar.setText(""+format_s.format(c.getTime()));
                tv_tgl_bayar_kirim.setText(""+format.format(c.getTime()));

                ll_tgl_pembayaran.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final View dialogView = View.inflate(Dashboard.this, R.layout.date_picker, null);
                        final AlertDialog alertDialog = new AlertDialog.Builder(Dashboard.this).create();
                        final DatePicker dp_awal = (DatePicker) dialogView.findViewById(R.id.date_picker);
                        dp_awal.setMinDate(System.currentTimeMillis());

                        dialogView.findViewById(R.id.date_time_set).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Calendar cal = new GregorianCalendar(dp_awal.getYear(),
                                        dp_awal.getMonth(),
                                        dp_awal.getDayOfMonth());

                                SimpleDateFormat format_show = new SimpleDateFormat("dd MMM yyyy");
                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                                Log.i(GlobalConfig.TAG, "tanggal : "+format_show.format(cal.getTime()));
                                tv_tgl_bayar.setText(format_show.format(cal.getTime()));
                                tv_tgl_bayar_kirim.setText(format.format(cal.getTime()));

                                alertDialog.dismiss();
                            }});
                        alertDialog.setView(dialogView);
                        alertDialog.show();
                    }
                });

                alertDialogBuilder.setCancelable(false)
                        .setPositiveButton("BAYAR",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //Log.d(getLocalClassName(), tv_jumlah_pembayaran.getText().toString()+" | "+tv_faktur.getText().toString());
                                        bayarPiutang(Integer.parseInt(tv_jumlah_pembayaran.getText().toString()), tv_tgl_bayar_kirim.getText().toString(), tv_faktur_pembayaran.getText().toString() );
                                        dialog.cancel();
                                    }
                                })
                        .setNegativeButton("BATAL",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                // create an alert dialog
                alert_detail_piutang = alertDialogBuilder.create();
                alert_detail_piutang.show();
            }
        });

        return data_history_view;
    }
    private void bayarPiutang(int jumlah, String tgl, String faktur){
        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_BAYAR_PELUNASAN;
        //Log.d(GlobalConfig.TAG,""+url);

        loading = ProgressDialog.show(Dashboard.this, "", "Mohon tunggu...", true);
        //bisa order
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.UP_KODE, pref.getString(GlobalConfig.GT_KODE, ""));
            jsonBody.put(GlobalConfig.USER_REGID, pref.getString(GlobalConfig.USER_REGID, ""));

            jsonBody.put(GlobalConfig.UP_TGL_BAYAR, tgl);
            jsonBody.put(GlobalConfig.UP_JUMLAH_BAYAR, jumlah);
            jsonBody.put(GlobalConfig.UP_FAKTUR_BAYAR, faktur);
            jsonBody.put(GlobalConfig.GKDSALES, pref.getString(GlobalConfig.GKDSALES, ""));
            //Log.d(GlobalConfig.TAG, jsonBody.toString());
            request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    //Log.d(GlobalConfig.TAG, response.toString());
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            notifikasi_dialog("", message);
                            if(alert_detail_piutang != null){
                                Log.d(getLocalClassName(), "dismiss");
                                alert_detail_piutang.dismiss();
                                alert_detail_piutang.cancel();
                            }
                        }else{
                            notifikasi(message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //Log.d("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loading.dismiss();
                    NetworkResponse response = error.networkResponse;
                    if(response != null && response.data != null){
                        Log.d(GlobalConfig.TAG, "code"+response.statusCode);
                        switch(response.statusCode){
                            case 404:
                                notifikasi("Terjadi masalah dengan server.");
                                break;
                            case 408:
                                notifikasi("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                                break;
                            case 500:
                                notifikasi("Terjadi masalah dengan server.");
                                break;
                            default:
                                notifikasi("Mohon maaf terjadi kesalahan.");
                                break;
                        }
                    }
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void notifikasi_dialog(String title, String message){
        if(title.equals("")){
            title = getResources().getString(R.string.app_name);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(Dashboard.this);
        builder.setTitle(title)
                .setCancelable(false)
                .setMessage(message)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}