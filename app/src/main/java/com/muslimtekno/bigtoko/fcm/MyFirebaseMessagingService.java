package com.muslimtekno.bigtoko.fcm;

/**
 * Created by imamudin on 27/01/17.
 */
/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.muslimtekno.bigtoko.DetailPembayaran;
import com.muslimtekno.bigtoko.R;
import com.muslimtekno.bigtoko.UlangTahun;
import com.muslimtekno.bigtoko.config.GlobalConfig;
import com.muslimtekno.bigtoko.model.Pembayaran;
import com.muslimtekno.bigtoko.mysp.ObscuredSharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    public static String GROUP_KEY_NOTIFICATION = "Pesanan Barus";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            ObscuredSharedPreferences pref = new ObscuredSharedPreferences(this,
                    this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );
            if(pref.getBoolean(GlobalConfig.IS_LOGIN, false)){
                JSONObject bayar = null;
                int tipe  = Integer.parseInt(remoteMessage.getData().get("tipe"));
                try {
                    if(tipe == 1) { //untuk notifikasi pembayaran baru
                        bayar = new JSONObject(remoteMessage.getData().get("body").toString());


                        Pembayaran pembayaran = new Pembayaran();
                        pembayaran.pembayaranJson = bayar.toString();
                        pembayaran.NAMA_PELANGGAN = bayar.getString(GlobalConfig.PNAMA_PELANGGAN);
                        pembayaran.ALAMAT_PELANGGAN = bayar.getString(GlobalConfig.PALAMAT_PELANGGAN);
                        pembayaran.PEMBAYARAN = bayar.getString(GlobalConfig.PPEMBAYARAN);
                        pembayaran.NO_KUITANSI = bayar.getString(GlobalConfig.PNO_KUITANSI);
                        pembayaran.TGL_KUITANSI = bayar.getString(GlobalConfig.PTGL_KUITANSI);
                        pembayaran.NO_FAKTUR = bayar.getString(GlobalConfig.PNO_FAKTUR);
                        pembayaran.TGL_FAKTUR = bayar.getString(GlobalConfig.PTGL_FAKTUR);
                        pembayaran.TOTAL = bayar.getString(GlobalConfig.PTOTAL);

                        sendBroadcast(new Intent(GlobalConfig.RECEIVER_BROADCAST_MAIN));
                        sendNotification(pembayaran.TGL_KUITANSI, remoteMessage.getData().get("body").toString());
                    }else if(tipe == 2){ //untuk notifikasi jenis ulang tahun
                        JSONObject data =new JSONObject(remoteMessage.getData().get("body").toString());

                        String kode     = data.getString("KODE");
                        String message  = data.getString("message");
                        String usia     = data.getString("USIA");
                        sendBroadcast(new Intent(GlobalConfig.RECEIVER_BROADCAST_MAIN));
                        sendNotificationUlangTahun(usia, message);
                    }

                } catch (JSONException e) {
                    Log.d(getPackageName(),"error : "+e.getMessage());
                    e.printStackTrace();
                }
            }
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String kuitansi, String messageBody) {
        Random random = new Random();
        int id_notifikasi = random.nextInt(9999 - 1000) + 1000;

        NotificationManager mNotificationManager = (NotificationManager) getApplicationContext()
                .getSystemService(getApplicationContext().NOTIFICATION_SERVICE);

        Intent intent1 = new Intent(this.getApplicationContext(),
                DetailPembayaran.class);
        intent1.putExtra("data", messageBody);
        intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingNotificationIntent = PendingIntent.getActivity(
                this.getApplicationContext(), id_notifikasi, intent1,
                PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

        Uri alarmSound = RingtoneManager
                .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_stat_name)
                        .setContentTitle("Semen Gresik | Pembayaran Pelanggan")
                        .setContentText("Tgl Pembayaran : "+kuitansi)
                        .setGroup(GROUP_KEY_NOTIFICATION)
                        .setSound(alarmSound)
                        .setLights(Color.WHITE, 500, 500)
                        .setAutoCancel(true).setTicker(getString(R.string.app_name))
                        .setStyle(new NotificationCompat.InboxStyle()
                                .addLine("Tgl Pembayaran : "+kuitansi))
                        .setVibrate(new long[]{100, 250, 100, 250, 100, 250})
                        .setContentIntent(pendingNotificationIntent);
        ///                        .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))

        mNotificationManager.notify(id_notifikasi, mBuilder.build());
    }
    private void sendNotificationUlangTahun(String usia, String message){
        Random random = new Random();
        int id_notifikasi = random.nextInt(9999 - 1000) + 1000;

        NotificationManager mNotificationManager = (NotificationManager) getApplicationContext()
                .getSystemService(getApplicationContext().NOTIFICATION_SERVICE);

        Intent intent1 = new Intent(this.getApplicationContext(),
                UlangTahun.class);
        intent1.putExtra("data", message);
        intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingNotificationIntent = PendingIntent.getActivity(
                this.getApplicationContext(), id_notifikasi, intent1,
                PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

        Uri alarmSound = RingtoneManager
                .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_stat_name)
                        .setContentTitle("Semen Gresik | Hari Jadi")
                        .setContentText(message)
                        .setGroup(GROUP_KEY_NOTIFICATION)
                        .setSound(alarmSound)
                        .setLights(Color.WHITE, 500, 500)
                        .setAutoCancel(true).setTicker(getString(R.string.app_name))
                        .setStyle(new NotificationCompat.InboxStyle()
                                .addLine(message))
                        .setVibrate(new long[]{100, 250, 100, 250, 100, 250})
                        .setContentIntent(pendingNotificationIntent);
        ///                        .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))

        mNotificationManager.notify(id_notifikasi, mBuilder.build());
    }
}