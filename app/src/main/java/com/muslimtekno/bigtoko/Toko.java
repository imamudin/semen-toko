package com.muslimtekno.bigtoko;

/**
 * Created by imamudin on 12/01/17.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.muslimtekno.bigtoko.app.MyAppController;
import com.muslimtekno.bigtoko.config.GlobalConfig;
import com.muslimtekno.bigtoko.mysp.ObscuredSharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Hashtable;
import java.util.Map;

public class Toko extends AppCompatActivity {
    LinearLayout ll_main;
    Button btn_inputStok, btn_order, btn_selesai;
    TextView tv_nama_toko, tv_poin, tv_alamat, tv_piutang, tv_jatuh_tempo;
    ObscuredSharedPreferences pref;
    ProgressDialog loading;
    JsonObjectRequest request;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.toko);

        init();
    }
    private void init(){
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Data Toko (pelanggan)");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        pref = new ObscuredSharedPreferences(Toko.this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );

        btn_inputStok   = (Button)findViewById(R.id.btn_input_stok);
        btn_order       = (Button)findViewById(R.id.btn_order);
        btn_selesai     = (Button)findViewById(R.id.btn_selesai);

        tv_alamat       = (TextView)findViewById(R.id.tv_alamat);
        tv_nama_toko    = (TextView)findViewById(R.id.tv_nama_toko);
        tv_piutang      = (TextView)findViewById(R.id.tv_piutang);
        tv_poin         = (TextView)findViewById(R.id.tv_poin);
        tv_jatuh_tempo  = (TextView)findViewById(R.id.tv_jatuh_tempo);

        tv_nama_toko.setText(pref.getString(GlobalConfig.GTOKO_NAMA,""));
        tv_alamat.setText(pref.getString(GlobalConfig.GTOKO_ALAMAT,""));
        tv_poin.setText(pref.getString(GlobalConfig.GTOKO_POIN,""));
        tv_piutang.setText(decimalToRupiah(Double.parseDouble(pref.getString(GlobalConfig.GTOKO_PIUTANG_KAP,"0"))));
        tv_jatuh_tempo.setText(pref.getString(GlobalConfig.GTOKO_TGL_JATUH_TEMPO,""));

        btn_inputStok.setOnClickListener(btnClick);
        btn_order.setOnClickListener(btnClick);
        btn_selesai.setOnClickListener(btnClick);

        ll_main     = (LinearLayout)findViewById(R.id.ll_main);
    }

    View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent open = null;
            if(v==btn_inputStok){
                mengecekFakturInputStok();
                return;
                //open = new Intent(TokoModel.this, InputStok.class);
            }else if(v==btn_order){
                //request harga dulu
                mengecekFaktur();
                return;
                //open = new Intent(TokoModel.this, FormOrder.class);
            }else if(v == btn_selesai){
                onBackPressed();
                return;
            }

            if(open != null){
                startActivity(open);
            }else{
                notifikasi("Mohon maaf terjadi kesalahan.");
            }
        }
    };
    private String decimalToRupiah(double harga){
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);

        String hasil = kursIndonesia.format(harga);
        return hasil.substring(0, (hasil.length()-3));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //menu.add(1, 2, 1, "Hapus Filter").setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void notifikasi(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
    }
    private void notifikasi_dialog(String title, String message){
        if(title.equals("")){
            title = getResources().getString(R.string.app_name);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(Toko.this);
        builder.setTitle(title)
                .setCancelable(false)
                .setMessage(message)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    @Override
    public void onBackPressed()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(Toko.this);
        // Set the dialog title
        builder.setTitle(getResources().getString(R.string.app_name))
                .setMessage("Apakah anda telah selesai melakukan kunjungan?")
                .setCancelable(false)
                .setPositiveButton("Selesai", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        tutupKunjungan();
                    }
                })
                .setNegativeButton("Belum", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    private void mengecekFakturInputStok(){
        Log.d(GlobalConfig.TAG, "make progress dialog2");
        loading = ProgressDialog.show(Toko.this, "Menyiapkan Form", "Mohon tunggu...", true);
        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_FORMSTOK;
        //Log.d(GlobalConfig.TAG,""+url);
        JSONObject jsonBody = null;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                loading.dismiss();
                Log.d(GlobalConfig.TAG, "tutup loading");
                try {
                    int status      = response.getInt("status");
                    String message  = response.getString("message");
                    if(status==1){
                        JSONObject data     = response.getJSONObject("data");
                        JSONArray dproduk  = data.getJSONArray("produk");
                        Intent open = new Intent(Toko.this, InputStok.class);
                        open.putExtra(GlobalConfig.GPRODUK, dproduk.toString());
                        startActivity(open);
                    }else{
                        notifikasi(message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Log.d("respons",response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                NetworkResponse response = error.networkResponse;
                if(response != null && response.data != null){
                    Log.d(GlobalConfig.TAG, "code"+response.statusCode);
                    switch(response.statusCode){
                        case 404:
                            displayMessage("Terjadi masalah dengan server.");
                            break;
                        case 500:
                            displayMessage("Terjadi masalah dengan server.");
                            break;
                        case 408:
                            displayMessage("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                            break;
                        default:
                            displayMessage("Mohon maaf terjadi kesalahan.");
                            break;
                    }
                }
            }
        }){
            public Map<String, String> getHeaders() {
                Map<String,String> headers = new Hashtable<String, String>();

                //Adding parameters
                headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }};

        request.setRetryPolicy(new DefaultRetryPolicy(
                GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyAppController.getInstance().addToRequestQueue(request);
    }
    private void mengecekFaktur(){
        loading = ProgressDialog.show(Toko.this, "Mengecek Faktur", "Mohon tunggu...", true);
        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_FORMORDER;
        //Log.d(GlobalConfig.TAG,""+url);
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));       //mewaliki nik juga
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));
            jsonBody.put(GlobalConfig.ID_TOKO, pref.getString(GlobalConfig.ID_TOKO, ""));

            //Log.d(GlobalConfig.TAG, jsonBody.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    Log.d(GlobalConfig.TAG, "tutup loading");
                    try {
                        int status = response.getInt("status");
                        String message = response.getString("message");
                        if (status == 1) {
                            JSONObject data = response.getJSONObject("data");
                            JSONArray dharga = data.getJSONArray("harga");
                            JSONArray gudang = data.getJSONArray("gudang");
                            String default_gudang = data.getString(GlobalConfig.GDEFAULT_GUDANG);

                            double piutang = Double.parseDouble(data.getString(GlobalConfig.GTOKO_TOTAL_PIUTANG));
                            Intent open = new Intent(Toko.this, FormOrder.class);
                            open.putExtra(GlobalConfig.GHARGA, dharga.toString());
                            open.putExtra(GlobalConfig.GGUDANG, gudang.toString());
                            open.putExtra(GlobalConfig.GTOKO_TOTAL_PIUTANG, piutang);
                            open.putExtra(GlobalConfig.GDEFAULT_GUDANG, default_gudang);
                            startActivity(open);
                        } else {
                            notifikasi_dialog("",message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //Log.d("respons", response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loading.dismiss();
                    NetworkResponse response = error.networkResponse;
                    if (response != null && response.data != null) {
                        Log.d(GlobalConfig.TAG, "code" + response.statusCode);
                        switch (response.statusCode) {
                            case 404:
                                displayMessage("Terjadi masalah dengan server.");
                                break;
                            case 500:
                                displayMessage("Terjadi masalah dengan server.");
                                break;
                            case 408:
                                displayMessage("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                                break;
                            default:
                                displayMessage("Mohon maaf terjadi kesalahan.");
                                break;
                        }
                    }
                }
            }) {
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private void tutupKunjungan(){
        Log.d(GlobalConfig.TAG, "make progress dialog2");
        loading = ProgressDialog.show(Toko.this, "Menutup Kunjungan", "Mohon tunggu...", true);

        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_TUTUPKUNJUNGAN;
        //Log.d(GlobalConfig.TAG,""+url);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));       //mewaliki nik juga
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));

            jsonBody.put(GlobalConfig.ID_TOKO, pref.getString(GlobalConfig.ID_TOKO, ""));
            jsonBody.put(GlobalConfig.GKUNJUNGAN_TGL, pref.getString(GlobalConfig.GKUNJUNGAN_TGL, ""));
            jsonBody.put(GlobalConfig.GKUNJUNGAN_JAM_M, pref.getString(GlobalConfig.GKUNJUNGAN_JAM_M, ""));

            //Log.d(GlobalConfig.TAG,""+jsonBody.toString());
            request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    Log.d(GlobalConfig.TAG, "tutup loading");
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            //hapus semua preferences
                            //update database dulu, kemudian baru keluar
                            pref.edit().remove(GlobalConfig.IS_TOKO).commit();
                            pref.edit().remove(GlobalConfig.ID_TOKO).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_NAMA).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_ALAMAT).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_PEMILIK).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_NOTELP).commit();
                            pref.edit().remove(GlobalConfig.GREAL_PLAFON).commit();
                            pref.edit().remove(GlobalConfig.GKDSALES).commit();
                            pref.edit().remove(GlobalConfig.GJATUHTEMPOBAYAR).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_LAT).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_LONG).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_NIK).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_POIN).commit();
                            pref.edit().remove(GlobalConfig.ID_TOKO).commit();

                            pref.edit().remove(GlobalConfig.GTOKO_PIUTANG_KAP).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_TGL_JATUH_TEMPO).commit();

                            pref.edit().remove(GlobalConfig.GKUNJUNGAN_TGL).commit();
                            pref.edit().remove(GlobalConfig.GKUNJUNGAN_JAM_M).commit();
                            tutupActivity();
                        }else{
                            notifikasi(message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //Log.d("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loading.dismiss();
                    NetworkResponse response = error.networkResponse;
                    if(response != null && response.data != null){
                        Log.d(GlobalConfig.TAG, "code"+response.statusCode);
                        switch(response.statusCode){
                            case 404:
                                displayMessage("Terjadi masalah dengan server.");
                                break;
                            case 500:
                                displayMessage("Terjadi masalah dengan server.");
                                break;
                            case 408:
                                displayMessage("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                                break;
                            default:
                                displayMessage("Mohon maaf terjadi kesalahan.");
                                break;
                        }
                    }
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void tutupActivity(){
        AlertDialog.Builder builder = new AlertDialog.Builder(Toko.this);
        // Set the dialog title
        builder.setTitle(getResources().getString(R.string.app_name))
                .setMessage("Kunjungan telah selesai.\nTerima kasih.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        openDashboard();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    public void displayMessage(String toastString){
        Toast.makeText(getApplicationContext(), toastString, Toast.LENGTH_LONG).show();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelRequest();
    }
    private void cancelRequest(){
        if(request!=null) {
            MyAppController.getInstance().cancelPendingRequests(request);
        }
    }
    public void openDashboard(){
        Intent dashboard = new Intent (Toko.this, Dashboard.class);
        startActivity(dashboard);
        Toko.this.finish();
    }
}